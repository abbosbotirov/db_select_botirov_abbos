select 
	t.calendar_week_number as week_number,
	t.time_id as date_of,
	t.day_name as week_day_name,
	t.calendar_year as year,
	round(sum(s.amount_sold), 2) as sales_amount,
	round(avg(s.amount_sold) over (
		order by t.calendar_week_number asc, t.day_name asc
		rows between 1 preceding and 1 following
	), 2) as centered_3_day_avg,
	round(sum(s.amount_sold) over (
		order by t.calendar_week_number asc, t.day_name asc
	), 2) as CUM_SUM, 
	round(coalesce(avg(s.amount_sold) over (partition by t.calendar_week_number order by t.day_name asc rows between 1 preceding and 2 following)), 2) as Monday_avg,
	round(avg(s.amount_sold) over (partition by t.calendar_week_number order by t.day_name asc rows between 1 preceding and 3 following), 2) as Friday_avg
	from sh.times as t
	inner join sh.sales as s on t.time_id = s.time_id
	where t.calendar_year = '1999'
	and (t.calendar_week_number = '49' or t.calendar_week_number = '50' or t.calendar_week_number = '51')
	and t.time_id between '1999-01-01' and '1999-12-31'
	group by t.calendar_week_number, t.time_id, t.day_name, t.calendar_year, s.amount_sold;
	



	