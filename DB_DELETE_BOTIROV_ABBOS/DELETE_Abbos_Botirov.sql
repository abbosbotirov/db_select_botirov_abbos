
-- find the film that inserted
select film_id from film where title = 'Despicable me 4'

-- delete the inventory that contains film_id, it won't work first because in rental there is record that contains with this film_id
delete from inventory where film_id = 1001;

-- it deletes the rental record which contains the film_id
delete from rental where inventory_id in (select inventory_id from inventory where film_id = 1001);

-- select the customer with updated name
select * from customer where first_name = 'Abbos' and last_name = 'Botirov';

-- delete from other table
delete from payment where customer_id = 1;

select * from payment where customer_id = 1;

delete from rental where customer_id = 1;

select * from rental where customer_id = 1;



