create database ddltask;

create schema public;

create table customer(
	customer_id smallint generated always as identity primary key,
	first_name varchar(50),
	last_name varchar(50),
	email varchar(50),
	phone varchar(50),
	address varchar(255),
	city varchar(100),
	state varchar(50),
	postal_code int
);

create table orders(
	order_id smallint generated always as identity primary key,
	customer_id int references customer(customer_id),
	order_date date,
	total_amount decimal(10, 2),
	status varchar(50)
);

create table product(
	product_id smallint generated always as identity primary key,
	name varchar(100),
	description text,
	price decimal(10, 2)
);

create table order_items(
	order_id smallint references orders(order_id),
	product_id smallint references product(product_id),
	quantity int,
	subtotal decimal(10, 2),
	constraint order_items_pkey primary key(order_id, product_id)
);

alter table orders
	add constraint check_order_date check (order_date > '2000-01-01');
	
alter table order_items 
	add constraint check_quantity_non_negative check (quantity >= 0),
	add constraint check_subtotal_non_negative check (quantity >= 0);
	
alter table customer
	add constraint check_email_uniqueness unique(email);
	
alter table customer
	alter column first_name set not null,
	alter column last_name set not null,
	alter column email set not null;

alter table orders
	alter column order_date set not null,
	alter column total_amount set not null,
	alter column status set not null;
	
alter table product
	alter column name set not null,
	alter column price set not null;

insert into customer(first_name, last_name, email, phone, address, city, state, postal_code)
values(
	'John',
	'Doe',
	'john.doe@example.com',
	'123-456-7890',
	'123 Main St',
	'Anytown',
	'CA',
	'12345'
),
(
	'Jane',
	'Smith',
	'jane.smith@example.com',
	'987-654-3210',
	'456 Elm St',
	'Othertown',
	'Ny',
	'54321'
),
(
	'Michael',
	'Johnson',
	'michael.johnson@example.com',
	'555-555-5555',
	'789 Oak St',
	'Somewhere',
	'TX',
	'67890'
);

select * from customer;

insert into orders(customer_id, order_date, total_amount, status)
values 
	(1, '2022-03-15', 100.00, 'Completed'),
	(2, '2022-03-16', 150.00, 'Pending'),
	(3, '2022-03-17', 200.00, 'Shipped');
	
select * from orders;
	
insert into product(name, description, price)
values
	('Product A', 'Description for Product A', 10.99),
	('Product B', 'Description for Product B', 20.99),
	('Product C', 'Description for Product C', 30.99),
	('Product D', 'Description for Product D', 40.99);
	
select * from product;
	
insert into Order_items(order_id, product_id, quantity, subtotal)
values
	(1, 1, 2, 21.98),
	(1, 2, 1, 20.99),
	(2, 3, 3, 92.97),
	(3, 4, 2, 81.98);
	
select * from order_items;

alter table customer
	add column record_ts date default CURRENT_DATE;
	
select * from customer;
	
alter table orders
	add column record_ts date default CURRENT_DATE;
	
select * from orders;
	
alter table product
	add column record_ts date default CURRENT_DATE;
	
select * from product;
	
alter table order_items
	add column record_ts date default CURRENT_DATE;
	
select * from order_items;


