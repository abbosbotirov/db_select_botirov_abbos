WITH SubcategorySales AS (
    SELECT
        p.prod_subcategory,
        t.time_id,
        SUM(s.amount_sold) AS total_sales
    FROM
        sh.sales s
    JOIN
        sh.products p ON s.prod_id = p.prod_id
    JOIN
        sh.times t ON s.time_id = t.time_id
    WHERE
        t.time_id BETWEEN '1998-01-01' AND '2001-12-31'
    GROUP BY
        p.prod_subcategory,
        t.time_id
),
LaggedSales AS (
    SELECT
        prod_subcategory,
        time_id,
        total_sales,
        LAG(total_sales, 1, 0) OVER (PARTITION BY prod_subcategory ORDER BY time_id) AS previous_year_sales
    FROM
        SubcategorySales
)
SELECT DISTINCT
    prod_subcategory
FROM
    LaggedSales
WHERE
    total_sales > previous_year_sales
    AND time_id IN ('1999-12-31', '2000-12-31', '2001-12-31');