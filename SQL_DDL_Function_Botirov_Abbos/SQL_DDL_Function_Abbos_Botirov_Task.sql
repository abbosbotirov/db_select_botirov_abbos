create view sales_revenue_by_category_qtr as
select 
	c.name AS category,
    sum(p.amount) AS total_sales
from payment p
join rental r ON p.rental_id = r.rental_id
join inventory i ON r.inventory_id = i.inventory_id
join film f ON i.film_id = f.film_id
join film_category fc ON f.film_id = fc.film_id
join category c ON fc.category_id = c.category_id
where
	extract(quarter from p.payment_date) = extract(quarter from current_timestamp)
	and extract(year from p.payment_date) = extract(year from current_timestamp)
group by
	c.name
order by (sum(p.amount)) DESC;

select * from sales_revenue_by_category_qtr;

create or replace function get_sales_revenue_by_category_qtr(quarter int)
returns table (category varchar, total_revenue numeric)
language plpgsql as
$$
begin
	return QUERY
	select
		c.name::varchar as category,
		sum(p.amount) as total_revenue
	from payment p
	join rental r ON p.rental_id = r.rental_id
	join inventory i ON r.inventory_id = i.inventory_id
	join film f ON i.film_id = f.film_id
	join film_category fc ON f.film_id = fc.film_id
	join category c ON fc.category_id = c.category_id
	where
		extract(quarter from p.payment_date) = extract(quarter from current_timestamp)
		and extract(year from p.payment_date) = extract(year from current_timestamp)
	group by
		c.name
	having sum(p.amount) > 0;
end;
$$;

select * from get_sales_revenue_by_category_qtr(1);

create or replace function new_movie(movie_title varchar)
returns void
language plpgsql as
$$
declare lang_id int;
begin
	select language_id into lang_id from language where name = 'English';
	
	if lang_id is null then
		raise exception 'Language "English" does not exist in the language table.';
	end if;
	
	insert into film(title, language_id, rental_duration, rental_rate, replacement_cost, release_year)
	values(movie_title, lang_id, 3, 4.99, 19.99, extract(year from current_timestamp));
end;
$$;

select * from new_movie('Despicable me 4');

