-- Retrieve the total sales amount for each product category for a specific time period
SELECT
    p.prod_category,
    SUM(s.amount_sold) AS total_sales_amount
FROM sh.sales s
JOIN sh.products p ON s.prod_id = p.prod_id
JOIN sh.times t ON s.time_id = t.time_id
WHERE t.time_id BETWEEN '1999-01-01' AND '1999-12-31'
GROUP BY p.prod_category
ORDER BY total_sales_amount DESC;
	
-- Calculate the average sales quantity by region for a particular product
SELECT
    c.country_name,
    AVG(s.amount_sold) AS average_quantity_sold
FROM sh.sales s
JOIN sh.products p ON s.prod_id = p.prod_id
JOIN sh.customers cu ON s.cust_id = cu.cust_id
JOIN sh.countries c ON cu.country_id = c.country_id
WHERE p.prod_name = 'Y Box'
GROUP BY c.country_name
ORDER BY average_quantity_sold DESC;
	
-- Find the top five customers with the highest total sales amount
SELECT
    cu.cust_first_name,
    cu.cust_last_name,
    SUM(s.amount_sold) AS total_sales_amount
FROM sh.sales s
JOIN sh.customers cu ON s.cust_id = cu.cust_id
GROUP BY
    cu.cust_first_name,
    cu.cust_last_name
ORDER BY total_sales_amount DESC
LIMIT 5;


