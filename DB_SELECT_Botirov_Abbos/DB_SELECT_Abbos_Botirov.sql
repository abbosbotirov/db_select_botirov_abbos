
-- 1st task select total revenue of 2017 
select
	s.store_id,
	s.staff_id,
	CONCAT(s.first_name, ' ', s.last_name) as staff_name,
	SUM(p.amount) as total_revenue
from
	staff s
inner join
	payment p on s.staff_id = p.staff_id
where
	p.payment_date BETWEEN '2017-01-01' and '2017-12-31'
group by
	s.store_id,
	s.staff_id
HAVING
	SUM(p.amount) = (
		select
			max(total_revenue)
		from
			(
				SELECT
                    s.store_id,
                    s.staff_id,
                    SUM(p.amount) AS total_revenue
                FROM
                    staff s
                INNER JOIN
                    payment p ON s.staff_id = p.staff_id
                WHERE
                    p.payment_date BETWEEN '2017-01-01' AND '2017-12-31'
                GROUP BY
                    s.store_id,
                    s.staff_id
			) as subquery
		where
			subquery.store_id = s.store_id
	);
	
-- 2nd task give top five movie rented most with average user's age
select
	f.title, 
	f.release_year, 
	count(r.rental_id) AS rental_count, 
    avg(extract(year from AGE(NOW(), c.create_date))) as expected_age
from 
	film f
join 
	inventory i on f.film_id = i.film_id
join 
	rental r on i.inventory_id = r.inventory_id
JOIN 
	customer c on r.customer_id = c.customer_id
group by 
	f.film_id, f.title, f.release_year
order by 
	rental_count desc
limit 5;

-- 3rd task choose the actor's inactivity and count it compared to current time
select 
	CONCAT(a.first_name, ' ', a.last_name) as actor_name, 
    max(NOW() - fa.last_update) as days_since_last_role
from 
	actor a
left join 
	film_actor fa on a.actor_id = fa.actor_id
group by 
	a.actor_id
order by 
	days_since_last_role desc;