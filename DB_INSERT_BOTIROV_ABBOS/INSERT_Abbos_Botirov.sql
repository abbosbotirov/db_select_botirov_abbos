insert into film(title, description, release_year, language_id, rental_duration, rental_rate, replacement_cost, rating, last_update)
values(
	'Despicable me 4',
	'Just another part journey of Gru and his family',
	2024,
	1,
	2,
	4.99,
	21.00,
	'PG-13',
	CURRENT_TIMESTAMP
);

select * from film where title = 'Despicable me 4';

insert into actor(first_name, last_name, last_update)
values('Steve', 'Carell', CURRENT_TIMESTAMP);

insert into actor(first_name, last_name, last_update)
values('Kristen', 'Wiig', CURRENT_TIMESTAMP);

insert into actor(first_name, last_name, last_update)
values('Joi', 'King', CURRENT_TIMESTAMP);

insert into actor(first_name, last_name, last_update)
values('Will', 'Ferrel', CURRENT_TIMESTAMP);

select * from actor;

insert into film_actor(actor_id, film_id, last_update)
select actor_id, (select film_id from film where title = 'Despicable me 4'), CURRENT_TIMESTAMP
from actor
where first_name = 'Steve' and last_name = 'Carell';

insert into film_actor(actor_id, film_id, last_update)
select actor_id, (select film_id from film where title = 'Despicable me 4'), CURRENT_TIMESTAMP
from actor
where first_name = 'Kristen' and last_name = 'Wiig';

insert into film_actor(actor_id, film_id, last_update)
select actor_id, (select film_id from film where title = 'Despicable me 4'), CURRENT_TIMESTAMP
from actor
where first_name = 'Joi' and last_name = 'King';

insert into film_actor(actor_id, film_id, last_update)
select actor_id, (select film_id from film where title = 'Despicable me 4'), CURRENT_TIMESTAMP
from actor
where first_name = 'Will' and last_name = 'Ferrel';

select * from film_actor where film_id = 1001;

insert into inventory (film_id, store_id, last_update)
values(1001, 1, CURRENT_TIMESTAMP);

insert into rental(rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
select CURRENT_DATE, inventory_id, (select customer_id from customer where customer_id = 1), CURRENT_TIMESTAMP, (select staff_id from staff where staff_id = 1), CURRENT_TIMESTAMP
from inventory;

select * from inventory where film_id = 1001;

select * from rental where inventory_id = 4585;