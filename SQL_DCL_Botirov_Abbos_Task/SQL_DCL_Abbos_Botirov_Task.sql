create or replace user rentaluser with password 'rentalpassword';

grant connect on database dvdrental to rentaluser;


grant select on customer to rentaluser;

select * from customer;


create role rental;

grant rental to rentaluser;


grant insert, update on rental to rental;

set role rental;

insert into rental(rental_date, inventory_id, customer_id, return_date, staff_id)
values('2024-03-29', 1, 1, '2024-04-05', 1);

update rental
set return_date = '2024-03-30'
where rental_id = 1;

reset role;



revoke insert on rental from rental;

set role rental;

insert into rental(rental_date, inventory_id, customer_id, return_date, staff_id)
values('2024-03-29', 2, 4, '2024-04-05', 1);

reset role;



select customer_id, first_name, last_name
from customer
where customer_id in (
	select distinct customer_id
	from payment
) and customer_id in (
	select distinct customer_id
	from rental
);


do
$$
declare
	cust_rec record;
	role_name varchar;
begin
	for cust_rec in
		select customer_id, first_name, last_name
		from customer
		where customer_id in(
			select distinct customer_id
			from payment
		) and customer_id in(
			select distinct customer_id
			from rental
		)
	loop
		role_name := 'client_' || cust_rec.first_name || '_' || cust_rec.last_name;
		execute 'CREATE ROLE ' || role_name;
	end loop;
end;
$$;



do
$$
declare
	cust_rec record;
begin
	for cust_rec in
		select customer_id, first_name, last_name
		from customer
		where customer_id in (
			select distinct customer_id
			from payment
		) and customer_id in (
			select distinct customer_id
			from rental
		)
	loop
		execute 'GRANT SELECT ON payment TO client_' || cust_rec.first_name || '_' || cust_rec.last_name;
		execute 'GRANT SELECT ON rental TO client_' || cust_rec.first_name || '_' || cust_rec.last_name;
		execute 'ALTER DEFAULT PRIVILEGES FOR ROLE client_' || cust_rec.first_name || '_' || cust_rec.last_name || 
		' IN SCHEMA public GRANT SELECT ON TABLES TO client_' || cust_rec.first_name || '_' || cust_rec.last_name;
	end loop;
end;
$$;

set role to client_aaron_selby;
select * from payment;
select * from rental;
reset role;
