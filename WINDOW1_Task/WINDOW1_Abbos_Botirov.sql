WITH RankedCustomers AS (
    SELECT
        cust_id,
        RANK() OVER (ORDER BY SUM(amount_sold) DESC) AS rank
    FROM sh.sales
    WHERE
        time_id BETWEEN '1998-01-01' AND '2001-12-31'
    GROUP BY
        cust_id
),
TopCustomers AS (
    SELECT
        cust_id
    FROM RankedCustomers
    WHERE
        rank <= 300
)
SELECT
    ch.channel_desc,
    tc.cust_id,
    c.cust_first_name,
    c.cust_last_name,
    SUM(s.amount_sold) AS total_sales
FROM
    TopCustomers tc
JOIN
    sh.sales s ON tc.cust_id = s.cust_id
JOIN
    sh.channels ch ON s.channel_id = ch.channel_id
JOIN
    sh.customers c ON tc.cust_id = c.cust_id
WHERE
    s.time_id BETWEEN '1998-01-01' AND '2001-12-31'
GROUP BY
    ch.channel_desc,
    tc.cust_id,
    c.cust_first_name,
    c.cust_last_name
ORDER BY
    ch.channel_desc,
    total_sales DESC;
	
	




	
	