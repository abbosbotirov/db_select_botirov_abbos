update film
set rental_duration = 21, rental_rate = 9.99
where title = 'Despicable me 4';

select * from film;


-- changes the customer name for customer_id = 1 because as you can see down it chose the customer whose records is higher or equal to 10
-- and changed the create_date to current_date
UPDATE customer
SET first_name = 'Abbos', 
    last_name = 'Botirov', 
    email = 'your.email@example.com', 
    address_id = (SELECT address_id FROM address ORDER BY RANDOM() LIMIT 1), 
    activebool = true, 
    create_date = CURRENT_DATE, 
    last_update = CURRENT_TIMESTAMP
WHERE customer_id = (
	SELECT c.customer_id
    FROM customer c
    JOIN rental r ON c.customer_id = r.customer_id
    JOIN payment p ON c.customer_id = p.customer_id
    GROUP BY c.customer_id
    HAVING COUNT(DISTINCT r.rental_id) >= 10 AND COUNT(DISTINCT p.payment_id) >= 10
    LIMIT 1
);

select * from customer;

select * from rental where customer_id = 1;

select * from payment where customer_id = 1;